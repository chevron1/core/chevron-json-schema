package org.chevron.json;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.loader.SchemaClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JsonSchemaGateway implements SchemaClient {

    private List<JsonSchemaSource> schemaSources = new ArrayList<>();
    private static final Logger LOGGER = LogManager.getLogger(JsonSchemaGateway.class);

    public void registerSource(JsonSchemaSource source){
        this.schemaSources.add(source);
    }

    public Optional<InputStream> getByName(String name){
        for(JsonSchemaSource source: schemaSources) {
            if (source.has(name)) {
                return source.getSchema(name);
            }
        }
        return  Optional.empty();
    }

    public InputStream get(String location){
        if(location.startsWith(Constants.LOCAL_DEFINITIONS_URI_SCHEME)){
            //Local definition
            String name = location.substring(Constants.LOCAL_DEFINITIONS_URI_SCHEME.length());
            LOGGER.debug("Loading a local object schema: [{}]",name);
            Optional<InputStream> optionalSchemaStream =  getByName(name);
            if(optionalSchemaStream.isPresent()) {
                LOGGER.debug("Successfully opened local object schema stream");
                return optionalSchemaStream.get();
            }
            else return null;
        }
        //Remote definition
        try{
            LOGGER.debug("Loading remote object schema: [{}]",location);
            return new URL(location).openStream();
        }catch (IOException ex){
            throw new ChevronJsonException("error loading JSON schema from remote location: "+location,
                    ex);
        }
    }

}
