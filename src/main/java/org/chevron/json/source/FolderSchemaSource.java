package org.chevron.json.source;

import org.chevron.json.ChevronJsonException;
import org.chevron.json.JsonSchemaSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class FolderSchemaSource implements JsonSchemaSource {

    private File file;

    public FolderSchemaSource(File file){
        if(file==null||!file.exists()||file.isFile())
            throw new IllegalArgumentException("file must point to an existing directory");
        this.file = file;
    }

    private String makeSchemaFilePath(String name){

        return file.getAbsolutePath()+File.separator+name+
                ".json";

    }

    public boolean has(String name){

        return new File(makeSchemaFilePath(name))
                .exists();

    }

    public Optional<InputStream> getSchema(String name){
        File schemaFile = new File(makeSchemaFilePath(name));
        if(!schemaFile.exists())
            return Optional.empty();
        try {
            return Optional.of(new FileInputStream(schemaFile));
        }catch (IOException ex){
            throw new ChevronJsonException("error loading schema file: "+schemaFile.getAbsolutePath(),
                    ex);
        }
    }

}
