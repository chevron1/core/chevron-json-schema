package org.chevron.json.source;

import org.chevron.json.JsonSchemaSource;

import java.io.InputStream;
import java.util.Optional;

public class ClasspathSchemaSource implements JsonSchemaSource {

    private String path;
    private ClassLoader classLoader;

    public ClasspathSchemaSource(String path){
        this(path,ClasspathSchemaSource.class.getClassLoader());
    }

    public ClasspathSchemaSource(String path, ClassLoader classLoader){
        if(path==null||path.isEmpty())
            throw new IllegalArgumentException("path must not be null nor empty");
        if(classLoader==null)
            throw new IllegalArgumentException("classloader must not be null");
        this.path = path;
        this.classLoader = classLoader;
    }

    private String makeResourcePath(String name){

        return path+"/"+name+".json";

    }

    public boolean has(String name){
        String resourcePath = makeResourcePath(name);
        return this.classLoader.getResource(resourcePath) != null;
    }

    public Optional<InputStream> getSchema(String name){
        return Optional.ofNullable(this.classLoader
                .getResourceAsStream(makeResourcePath(
                        name)));
    }

}
