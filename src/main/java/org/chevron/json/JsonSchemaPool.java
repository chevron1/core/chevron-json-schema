package org.chevron.json;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.chevron.model.ObjectSchemasPool;
import org.chevron.model.schema.ObjectSchema;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.InputStream;
import java.util.*;

public class JsonSchemaPool implements ObjectSchemasPool {

    private boolean cachingEnabled = false;
    private Map<String,JsonSchema> jsonSchemaMap = new HashMap<>();
    private JsonSchemaGateway jsonSchemaGateway = new JsonSchemaGateway();
    private JsonSchema voidSchema = new JsonSchema(null);

    private static final Logger LOGGER = LogManager.getLogger(JsonSchemaPool.class);

    public static final class JsonSchemasPoolBuilder {

        private JsonSchemasPoolBuilder(){

        }

        private JsonSchemaPool pool = new JsonSchemaPool();

        public JsonSchemasPoolBuilder enlist(JsonSchemaSource source){
            this.pool.jsonSchemaGateway.registerSource(
                    source);
            return this;
        }

        public JsonSchemasPoolBuilder disableCaching(){
            this.pool.cachingEnabled = false;
            return this;
        }

        public JsonSchemasPoolBuilder enableCaching(){
            this.pool.cachingEnabled = true;
            return this;
        }

        public JsonSchemaPool build(){
            return this.pool;
        }

    }

    public static JsonSchemasPoolBuilder builder(){

        return new JsonSchemasPoolBuilder();

    }

    private JsonSchemaPool(){}

    private JsonSchema loadSchema(InputStream stream){
        JSONObject rawSchema = new JSONObject(new JSONTokener(stream));
        Schema schema = SchemaLoader.builder()
                .resolutionScope(Constants.LOCAL_DEFINITIONS_URI_SCHEME)
                .schemaClient(jsonSchemaGateway)
                .schemaJson(rawSchema)
                .build().load()
                .build();
        return new JsonSchema(schema);
    }

    @Override
    public Optional<ObjectSchema> getSchema(String name) {
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        if(name.equalsIgnoreCase(ObjectSchema.VOID)) {
            LOGGER.debug("Returning void Object Schema");
            return Optional.of(voidSchema);
        }
        JsonSchema jsonSchema = null;
        if(cachingEnabled) {
            LOGGER.debug("Attempting to fetch object schema from cache");
            jsonSchema = jsonSchemaMap.get(name);
            if (jsonSchema != null) {
                LOGGER.debug("Object schema found in cache");
                return Optional.of(jsonSchema);
            }else{
                LOGGER.debug("Object schema not found in cache");
            }
        }
        Optional<InputStream> optionalSchemaStream = jsonSchemaGateway.getByName(name);
        if(!optionalSchemaStream.isPresent())
            return Optional.empty();
        jsonSchema = loadSchema(optionalSchemaStream.get());
        if(cachingEnabled) {
            LOGGER.debug("Putting Object schema into cache");
            jsonSchemaMap.put(name, jsonSchema);
        }
        return Optional.of(jsonSchema);
    }

}
