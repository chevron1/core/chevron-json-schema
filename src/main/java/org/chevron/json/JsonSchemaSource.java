package org.chevron.json;

import java.io.InputStream;
import java.util.Optional;

public interface JsonSchemaSource {

    boolean has(String name);
    Optional<InputStream> getSchema(String name);

}
