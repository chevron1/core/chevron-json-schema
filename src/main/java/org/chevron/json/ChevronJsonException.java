package org.chevron.json;

import org.chevron.model.ObjectSchemasPoolException;

public class ChevronJsonException extends ObjectSchemasPoolException {

    public ChevronJsonException(String message) {
        super(message);
    }

    public ChevronJsonException(String message, Throwable cause) {
        super(message, cause);
    }
}
