package org.chevron.json;

import org.chevron.json.source.FolderSchemaSource;
import org.chevron.model.schema.ObjectSchema;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Example {

    public static void main(String[] args){

        JsonSchemaPool pool = JsonSchemaPool.builder()
                .enlist(new FolderSchemaSource(new File("examples")))
                .build();

        Optional<ObjectSchema> optionalObjectSchema = pool.getSchema("person");
        System.out.println(optionalObjectSchema.isPresent());

        optionalObjectSchema = pool.getSchema("coordinates");
        System.out.println(optionalObjectSchema.isPresent());
        ObjectSchema coordinatesSchema = optionalObjectSchema.get();
        Map<String,Object> map = new HashMap<>();
        map.put("latitude",50);
        map.put("longitude",80);
        coordinatesSchema.validate(map);

    }

}
