package org.chevron.json;

import org.chevron.core.composition.InvalidParamsException;
import org.chevron.model.schema.ObjectSchema;
import org.chevron.model.schema.ParamsValidationException;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.json.JSONObject;

import java.util.Map;

public class JsonSchema implements ObjectSchema {

    private Schema schema;

    JsonSchema(Schema schema){
        this.schema = schema;
    }

    @Override
    public void validate(Map<String, Object> params) {
        try {
            if(schema==null)
                return;
            this.schema.validate(JSONObject.wrap(params));
        }catch (ValidationException ex){
            throw new InvalidParamsException("invalid params provided to schema ["+schema.getTitle()+"]",
                    ex);
        }catch (Exception ex){
            throw new ParamsValidationException("unexpected error found while validating params",
                    ex);
        }
    }

}
